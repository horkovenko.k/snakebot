package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.*;
import java.awt.*;

import static com.codenjoy.dojo.snake.client.CellType.*;


public class BoardCloned {
    public int Width = 15, Height = 15;


    int[][] clone = new int[Width][Height];
    public java.util.List<String> way = new ArrayList<>();


    //Apple coordinates
    public Point appleCoordinates = new Point();

    //Stone coordinates
    public Point stoneCoordinates = new Point();

    public void initClone(Board board) {
        for (int i = 0; i < Height; i++) {
            for (int j = 0; j < Width; j++) {
                Elements check = board.getAt(j, i);

                int indexX = Height - i - 1;
                int indexY = j;

                if (check == Elements.BREAK) clone[indexX][indexY] = WALL.getValue();
                if (check == Elements.NONE) clone[indexX][indexY] = EMPTY.getValue();

                if (check == Elements.BAD_APPLE) clone[indexX][indexY] = STONE.getValue();
                if (check == Elements.GOOD_APPLE) clone[indexX][indexY] = APPLE.getValue();

                if (check == Elements.HEAD_UP) clone[indexX][indexY] = HEAD.getValue();
                if (check == Elements.HEAD_DOWN) clone[indexX][indexY] = HEAD.getValue();
                if (check == Elements.HEAD_RIGHT) clone[indexX][indexY] = HEAD.getValue();
                if (check == Elements.HEAD_LEFT) clone[indexX][indexY] = HEAD.getValue();

                if (check == Elements.TAIL_END_DOWN) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_END_LEFT) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_END_RIGHT) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_END_UP) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_HORIZONTAL) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_VERTICAL) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_LEFT_DOWN) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_LEFT_UP) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_RIGHT_DOWN) clone[indexX][indexY] = BODY.getValue();
                if (check == Elements.TAIL_RIGHT_UP) clone[indexX][indexY] = BODY.getValue();

                appleCoordinates.x = board.getApples().get(0).getX();
                appleCoordinates.y = Height - 1 - board.getApples().get(0).getY();

                stoneCoordinates.x = board.getStones().get(0).getX();
                stoneCoordinates.y = Height - 1 - board.getStones().get(0).getY();

                way.clear();

            }
        }


    }

    public int propagateWave(int mark, Board board) {
        for (int i = 0; i < Height; i++) {
            for (int j = 0; j < Width; j++) {
                if (clone[i][j] == EMPTY.getValue()) {
                    int North = CellTypeCheck(i - 1, j);
                    int South = CellTypeCheck(i + 1, j);
                    int West = CellTypeCheck(i, j - 1);
                    int East = CellTypeCheck(i, j + 1);

                    int min = North;
                    if (South < min) min = South;
                    if (West < min)  min = West;
                    if (East < min)  min = East;

                    if (min == mark) clone[i][j] = min + 1;
                }
            }
        }

//        if (board.getSnake().size() <= 30) {
//            return isAppleReached();
//        }
//
//        return isStoneReached();
        return isAppleReached();

    }

    public int CellTypeCheck(int y, int x) {
        // Check walls
        if (x < 0) return WALL.getValue();
        if (y < 0) return WALL.getValue();
        if (x >= Width) return WALL.getValue();
        if (y >= Height) return WALL.getValue();

        return clone[y][x];
    }

    public void backPropagate(Board board) {
        int i, j;

//        if (board.getSnake().size() <= 30) {
//            j = appleCoordinates.x;
//            i = appleCoordinates.y;
//        } else {
//            j = stoneCoordinates.x;
//            i = stoneCoordinates.y;
//        }

        j = appleCoordinates.x;
        i = appleCoordinates.y;

        int x = j, y = i;

        int step = 0;

        while (true) {
            int North = CellTypeCheck(i - 1, j);
            int South = CellTypeCheck(i + 1, j);
            int West = CellTypeCheck(i, j - 1);
            int East = CellTypeCheck(i, j + 1);

            int min = North;

            String direction = Direction.DOWN.toString();

            x = j;
            y = i - 1;

            if (South < min) {
                min = South;
                direction = Direction.UP.toString();
                x = j;
                y = i + 1;
            }

            if (West < min) {
                min = West;
                direction = Direction.RIGHT.toString();

                x = j - 1;
                y = i;
            }

            if (East < min) {
                min = East;
                direction = Direction.LEFT.toString();

                x = j + 1;
                y = i;
            }

            if (min == HEAD.getValue()) {
                way.add(direction);
                break;
            }
            else {
                i = y;
                j = x;
                way.add(direction);
            }
        }

        Collections.reverse(way);

    }

    public int isAppleReached() {
        if (
                clone[appleCoordinates.y + 1][appleCoordinates.x] != EMPTY.getValue() &&
                        clone[appleCoordinates.y + 1][appleCoordinates.x] != WALL.getValue() &&
                        clone[appleCoordinates.y + 1][appleCoordinates.x] != STONE.getValue() &&
                        clone[appleCoordinates.y + 1][appleCoordinates.x] != BODY.getValue()
        ) return 1;

        if (
                clone[appleCoordinates.y - 1][appleCoordinates.x] != EMPTY.getValue() &&
                        clone[appleCoordinates.y - 1][appleCoordinates.x] != WALL.getValue() &&
                        clone[appleCoordinates.y - 1][appleCoordinates.x] != STONE.getValue() &&
                        clone[appleCoordinates.y - 1][appleCoordinates.x] != BODY.getValue()
        ) return 1;

        if (
                clone[appleCoordinates.y][appleCoordinates.x + 1] != EMPTY.getValue() &&
                        clone[appleCoordinates.y][appleCoordinates.x + 1] != WALL.getValue() &&
                        clone[appleCoordinates.y][appleCoordinates.x + 1] != STONE.getValue() &&
                        clone[appleCoordinates.y][appleCoordinates.x + 1] != BODY.getValue()
        ) return 1;

        if (
                clone[appleCoordinates.y][appleCoordinates.x - 1] != EMPTY.getValue() &&
                        clone[appleCoordinates.y][appleCoordinates.x - 1] != WALL.getValue() &&
                        clone[appleCoordinates.y][appleCoordinates.x - 1] != STONE.getValue() &&
                        clone[appleCoordinates.y][appleCoordinates.x - 1] != BODY.getValue()
        ) return 1;

        return 0;
    }

    public int isStoneReached() {
        if (
                clone[stoneCoordinates.y + 1][stoneCoordinates.x] != EMPTY.getValue() &&
                        clone[stoneCoordinates.y + 1][stoneCoordinates.x] != WALL.getValue() &&
                        clone[stoneCoordinates.y + 1][stoneCoordinates.x] != STONE.getValue() &&
                        clone[stoneCoordinates.y + 1][stoneCoordinates.x] != BODY.getValue()
        ) return 1;

        if (
                clone[stoneCoordinates.y - 1][stoneCoordinates.x] != EMPTY.getValue() &&
                        clone[stoneCoordinates.y - 1][stoneCoordinates.x] != WALL.getValue() &&
                        clone[stoneCoordinates.y - 1][stoneCoordinates.x] != STONE.getValue() &&
                        clone[stoneCoordinates.y - 1][stoneCoordinates.x] != BODY.getValue()
        ) return 1;

        if (
                clone[stoneCoordinates.y][stoneCoordinates.x + 1] != EMPTY.getValue() &&
                        clone[stoneCoordinates.y][stoneCoordinates.x + 1] != WALL.getValue() &&
                        clone[stoneCoordinates.y][stoneCoordinates.x + 1] != STONE.getValue() &&
                        clone[stoneCoordinates.y][stoneCoordinates.x + 1] != BODY.getValue()
        ) return 1;

        if (
                clone[stoneCoordinates.y][stoneCoordinates.x - 1] != EMPTY.getValue() &&
                        clone[stoneCoordinates.y][stoneCoordinates.x - 1] != WALL.getValue() &&
                        clone[stoneCoordinates.y][stoneCoordinates.x - 1] != STONE.getValue() &&
                        clone[stoneCoordinates.y][stoneCoordinates.x - 1] != BODY.getValue()
        ) return 1;

        return 0;
    }

    public void print() {
        for (int i = 0; i < Height; i++) {
            for (int j = 0; j < Width; j++) {
                if (clone[i][j] == WALL.getValue()) System.out.print('X');
                if (clone[i][j] == EMPTY.getValue()) System.out.print(' ');

                if (clone[i][j] == STONE.getValue()) System.out.print('a');
                if (clone[i][j] == APPLE.getValue()) System.out.print('A');

                if (clone[i][j] == HEAD.getValue()) System.out.print('0');

                if (clone[i][j] == BODY.getValue()) System.out.print('Z');
                if (clone[i][j] < WALL.getValue() ) System.out.print((char) (clone[i][j] % 10 + 0x30));
            }
            System.out.println();
        }

    }

}
