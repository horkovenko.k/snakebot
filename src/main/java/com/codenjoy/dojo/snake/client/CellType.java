package com.codenjoy.dojo.snake.client;

public enum CellType {
    EMPTY(0xFFFF),
    HEAD(0),
    BODY(0xFFFF - 1),
    APPLE(0xFFFF - 2),
    STONE(0xFFFF - 3),
    WALL(0xFFFF - 4);

    private int value;


    public int getValue() {
        return value;
    }

    CellType(int value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
