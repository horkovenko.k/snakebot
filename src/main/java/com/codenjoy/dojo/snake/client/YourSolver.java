package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.RandomDice;

/**
 * User: Konstantin Horkovenko
 */

public class YourSolver implements Solver<Board> {

    private Board board;

    public BoardCloned boardCloned = new BoardCloned();

    public YourSolver(Dice dice) {
    }

    public void getPath() {
        boardCloned.initClone(board);

        int reached = 0;

        int mark = 0;
        while (reached == 0) {
            reached = boardCloned.propagateWave(mark, board);
            mark++;
        }

//        boardCloned.print();
        boardCloned.backPropagate(board);

    }

    @Override
    public String get(Board board) {
        this.board = board;
//        System.out.println(board.toString());

        getPath();

        //19:10
        //50925

        return boardCloned.way.get(0);
    }

    public static void main(String[] args) {
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
                "http://167.99.241.128/codenjoy-contest/board/player/3gtsinpm41v52xan5emp?code=126740350538166180",
                new YourSolver(new RandomDice()),
                new Board());
    }

}
